# This file is part of CloudControl.
#
# CloudControl is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CloudControl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CloudControl.  If not, see <http://www.gnu.org/licenses/>.


import logging
from functools import wraps

import libvirt

from cloudcontrol.node.utils import and_


logger = logging.getLogger(__name__)


def _virt_tag(func):
    """Catches libvirt related exception.

    Decorator used for tag declarations that interacts with libvirt.

    """
    @wraps(func)
    def decorated(handl):
        if not handl.virt_connected:
            return
        try:
            return func(handl)
        except libvirt.libvirtError:
            logger.exception('Unexpected libvirt error')
            handl.vir_con_restart()

    return decorated


def _check_virt_connected(func):
    """Check is libvirt is connected before caculating tag."""
    @wraps(func)
    def decorated(handl):
        if not handl.virt_connected:
            return
        return func(handl)

    return decorated


def vir_status(handl):
    """Local libvirt connection status."""
    return {True: 'connected', False: 'disconnected'}[handl.virt_connected]


# hypervisor related tags
def htype():
    """Hypervisor type."""
    # FIXME for other support
    return u'kvm'


@_check_virt_connected
def hv(handl):
    """Hypervisor name."""
    # What is the point of this tag ? if the information not already in a and id
    # ?
    return handl.hypervisor.name


def hvm():
    """Hardware virtualization enable."""
    # see
    # http://www.linux-kvm.org/page/FAQ#How_can_I_tell_if_I_have_Intel_VT_or_AMD-V.3F
    # or
    # http://www.cyberciti.biz/faq/linux-xen-vmware-kvm-intel-vt-amd-v-support/
    # if we are in a xen hypervisor we won't see vt in /proc/cpuinfo
    result = {True: u'yes', False: u'no'}

    if htype() == u'kvm':
        # findout in /proc/cpuinfo if all CPUs have virtualisation enabled
        return result[and_(
            set(
                'vmx',  # Intel VT
                'svm',  # AMD 
            ) & set(
                l.split(': ')[-1].split()
            ) for l in open('/proc/cpuinfo').readline() if l.startswith('Tags')
        )]

    return None


@_virt_tag
def hvver(handl):
    """Hypervisor version."""
    return handl.hypervisor.vir_con.getVersion()


@_virt_tag
def libvirtver(handl):
    """Version of running libvirt."""
    return handl.hypervisor.vir_con.getLibVersion()


# jobs
def rjobs():
    """Number of currently running jobs."""


# storage pools
@_check_virt_connected
def sto(handl):
    """Storage pool names."""
    return u' '.join(handl.hypervisor.storage.storages.iterkeys())


# Vm related tags
@_check_virt_connected
def nvm(handl):
    """Number of VMS in the current hypervisor."""
    return handl.hypervisor.vm_total


@_check_virt_connected
def vmpaused(handl):
    """Count of VMs paused."""
    return handl.hypervisor.vm_paused


@_check_virt_connected
def vmstarted(handl):
    """Count of VMs started."""
    return handl.hypervisor.vm_started


@_check_virt_connected
def vmstopped(handl):
    """Count of VMs Stopped."""
    return handl.hypervisor.vm_stopped


@_check_virt_connected
def cpurunning(handl):
    """CPU total used by running VMs on the hypervisor."""
    return sum(int(vm.tags['cpu'].value) for vm in
               handl.hypervisor.domains.itervalues() if vm.tags['cpu'].value and
              vm.state == 'running')


@_check_virt_connected
def cpualloc(handl):
    """CPU total used by all VMs on the hypervisor."""
    return sum(int(vm.tags['cpu'].value) for vm in
               handl.hypervisor.domains.itervalues() if vm.tags['cpu'].value)


@_check_virt_connected
def memrunning(handl):
    """Memory used by running VMs on the hypervisor."""
    return sum(int(vm.tags['mem'].value) for vm in
               handl.hypervisor.domains.itervalues() if vm.tags['mem'].value and
               vm.state == 'running')


@_check_virt_connected
def memalloc(handl):
    """Memory used by all VMs on the hypervisor."""
    return sum(int(vm.tags['mem'].value) for vm in
               handl.hypervisor.domains.itervalues() if vm.tags['mem'].value)
