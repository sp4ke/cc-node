# This file is part of CloudControl.
#
# CloudControl is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CloudControl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CloudControl.  If not, see <http://www.gnu.org/licenses/>.


import logging
import logging.config
from ConfigParser import SafeConfigParser
from StringIO import StringIO


logger = logging.getLogger(__name__)


class NodeConfigParser(object):
    """ConfigParser for ccnode config file."""
    def __init__(self, file_path):
        config = SafeConfigParser()
        config.read(file_path)

        config = dict(config.items('node'))

        # ccserver settings
        try:
            self.server_host = config['address']
        except KeyError:
            logger.error('cc-server address not specified in config file')
            raise
        self.server_port = int(config.get('port', 1984))
        try:
            self.server_user = config['login']
        except KeyError:
            logger.error('cc-server login not specified in config file')
            raise
        try:
            self.server_passwd = config['password']
        except KeyError:
            logger.error('cc-server password not specified in config file')
            raise

        self.logging_level = int(config.get('verbosity', 0))

        self.debug = config.get('debug', 'no')
        if self.debug in ('yes', '1', 'on', 'true'):
            self.debug = True
        else:
            if self.debug not in ('no', '0', 'off', 'false'):
                logger.error('Invalid value for debug in config file')
            self.debug = False

        self.logging_level = int(config.get('verbosity', 0))
        self.logging_output = 'console' if self.debug == True else 'syslog'

        # path settings
        self.jobs_store_path = config.get('jobs_store_path',
                                          '/var/lib/cc-node/jobs')
        # plugins persistance
        self.plugins_store_path = config.get('plugins_store_path',
                                             '/var/lib/cc-node/plugins')


def configure_logging(level, output):
    level = {
        0: 'ERROR', 1: 'WARNING',
        2: 'INFO', 3: 'DEBUG',
    }[level]
    output = dict(
        console="""
[handler_output]
class=StreamHandler
formatter=simpleFormatter
args=(sys.stderr,)

[formatter_simpleFormatter]
format=cc-node - %(asctime)s - %(name)s - %(levelname)s - %(message)s
""",
        syslog="""
[handler_output]
class=handlers.SysLogHandler
formatter=simpleFormatter
args=('/dev/log', handlers.SysLogHandler.LOG_DAEMON)

[formatter_simpleFormatter]
class=cloudcontrol.common.helpers.formatter.EncodingFormatter
format=cc-node - %(name)s - %(levelname)s - %(message)s
""",
    )[output]

    # create config parser for logging configuration
    logging.config.fileConfig(StringIO("""
[loggers]
keys=root,ccnode,cccommon,sjrpc

[handlers]
keys=output

[formatters]
keys=simpleFormatter

[logger_root]
level=ERROR
handlers=output

[logger_ccnode]
level=%(level)s
handlers=
qualname=cloudcontrol.node

[logger_cccommon]
level=%(level)s
handlers=
qualname=cloudcontrol.common

[logger_sjrpc]
level=ERROR
handlers=
qualname=sjrpc

%(output)s
    """ % dict(level=level, output=output)))
