# This file is part of CloudControl.
#
# CloudControl is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CloudControl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CloudControl.  If not, see <http://www.gnu.org/licenses/>.


"""Exceptions classes for ccnode."""


class CCNodeError(Exception):
    """Base exception class for cc-node."""
    pass


class PluginError(CCNodeError):
    """Exception related to plugin execution."""
    pass


class UndefinedDomain(CCNodeError):
    """Operation on a domain that does not exist was tried."""
    pass


class PoolStorageError(CCNodeError):
    """Pool or volume was not found."""
    pass


class TunnelError(CCNodeError):
    """Error occured during TunnelJob execution."""
    pass


class DRBDAllocationError(CCNodeError):
    """Cannot create DRBD volume."""
    pass


class DRBDError(CCNodeError):
    """Error occured during DRBDJob execution."""
    pass


class ConsoleError(CCNodeError):
    """Error relative to VM virtio console handling."""
    pass


class ConsoleAlreadyOpened(ConsoleError):
    """VM virtio console is already opened."""
    pass


class VMMigrationError(CCNodeError):
    """Error during live migration job."""
    pass


class JobError(CCNodeError):
    """General exception for a job."""
    pass


class RemoteExecutionError(CCNodeError):
    """Thrown when a remote command execution error occurs."""
    pass
