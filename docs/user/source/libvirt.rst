Libvirt hot migration compatibility
-----------------------------------

Should work:

From 0.8.8 to 0.9.8
From 0.9.8 to 0.9.12

Should not work:

From 0.9.12 to 0.9.8 (because of bad xml seclabel, event if set explicitly it
doesn't work)
From 0.9.8 to 0.8.8 (because of XML format compat)

Note for all versions:
When migrating, libvirt try to acquire some mutex on the domain and this can
block forever (0.8.8) or timeout (0.9.8 and +). Take a look at the node logs to
be sure.

In the later case, you must restart libvirt in order for the migration to work
properly.

What to do when live migration fails
------------------------------------

Check DRBD and remove if needed:

::

    # identify which DRBD volume needs to go down
    it-test-15.lab.fr.lan ~ 0 # cat /proc/drbd 
    version: 8.3.9 (api:88/proto:86-95)
    srcversion: CF228D42875CF3A43F2945A 
     0: cs:Connected ro:Primary/Primary ds:UpToDate/UpToDate C r-----
        ns:2097152 nr:0 dw:0 dr:2097352 al:0 bm:128 lo:0 pe:0 ua:0 ap:0 ep:1 wo:f oos:0

    # identify all LV (VM, copy and DRBD meta)
    it-test-15.lab.fr.lan ~ 0 # dmsetup ls
    vg-Test--Clone--root	(253, 5)
    vg-Test--Clone--root.copy	(253, 2)
    vg-Test--Clone--root.drbdmeta	(253, 1)
    vg-Etherpad--Etherpad	(253, 0)

    # reload VM LV table if needed
    it-test-15.lab.fr.lan ~ 0 # dmsetup table vg-Test--Clone--root.copy | dmsetup load vg-Test--Clone--root
    it-test-15.lab.fr.lan ~ 0 # dmsetup suspend vg-Test--Clone--root
    it-test-15.lab.fr.lan ~ 0 # dmsetup resume vg-Test--Clone--root

    # halt DRBD volume
    it-test-15.lab.fr.lan ~ 0 # drbdsetup /dev/drbd0 disconnect
    it-test-15.lab.fr.lan ~ 0 # drbdsetup /dev/drbd0 secondary
    it-test-15.lab.fr.lan ~ 0 # drbdsetup /dev/drbd0 detach
    it-test-15.lab.fr.lan ~ 0 # drbdsetup /dev/drbd0 down

    # remove copy DM
    it-test-15.lab.fr.lan ~ 0 # dmsetup remove vg-Test--Clone--root.copy

    # remove DRBD meta
    it-test-15.lab.fr.lan ~ 0 # lvremove /dev/vg/Test-Clone-root.drbdmeta 

    # /!\ on hypervisor where the VM is not present, also remove the VM LV
    it-test-15.lab.fr.lan ~ 0 # lvremove /dev/vg/Test-Clone-root 

    # check that libvirt ended the qemu process

    # refresh libvirt pools
    it-test-15.lab.fr.lan ~ 0 # virsh pool-refresh vg
