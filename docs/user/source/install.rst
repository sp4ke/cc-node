Installation
============

Installation
------------

Repository
..........

Install from Smartjog debian repository:

.. code-block:: text

    # smartjog
    deb http://debian.fr.smartjog.net/debian-smartjog/ squeeze smartjog


**Cloud Control node** provides two packages named ``cc-node`` and
``cc-node-hypervisor``. The last is just a meta package that provides
dependencies for running the **Cloud Control node** as an **hypervisor** (``KVM``).
The first contains all the **Cloud Control node** code base and can be installed
if you only need to run the node with the **host** role.

Install with your favorite package manager as root:

.. code-block:: bash

    # apt-get install cc-node

or

.. code-block:: bash

    # apt-get install cc-node-hypervisor


Configuration
-------------

Example configuration file:

``[ccserver]`` section:

    .. literalinclude:: ../../../etc/cc-node.conf
        :lines: 1-5

For logging section, see `Python documentation <http://docs.python.org/library/logging.config.html#configuration-file-format>`_.

Full default file (using syslog):

.. literalinclude:: ../../../etc/cc-node.conf
    :lines: 1-18, 20-30, 36, 38

