Jobs
====

Jobs manager
------------

.. autoclass:: cloudcontrol.node.jobs.JobManager
    :members:

Abstract jobs
-------------

.. autoclass:: cloudcontrol.node.jobs.BaseThreadedJob
    :members:

.. autoclass:: cloudcontrol.node.jobs.ForkedJob
    :members:

.. autoclass:: cloudcontrol.node.jobs.BaseIOJob
    :members:

Hypervisor jobs
---------------

.. automodule:: cloudcontrol.node.hypervisor.jobs
    :members:
