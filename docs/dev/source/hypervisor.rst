Hypervisor
==========

Hypervisor handler
------------------

.. autoclass:: cloudcontrol.node.hypervisor.Handler
    :members:

Hypervisor object
-----------------

.. autoclass:: cloudcontrol.node.hypervisor.Hypervisor
    :members:

Storage pools and volumes
-------------------------

.. autoclass:: cloudcontrol.node.hypervisor.StorageIndex
    :members:

.. autoclass:: cloudcontrol.node.hypervisor.Storage
    :members:

.. autoclass:: cloudcontrol.node.hypervisor.Volume
    :members:
