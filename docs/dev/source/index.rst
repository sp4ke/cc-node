.. Cloud Control node documentation master file, created by
   sphinx-quickstart on Thu Sep 15 15:48:00 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cloud Control node's documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2

   architecture
   tags
   plugins
   host
   libvirt
   hypervisor
   vm
   jobs
   cold_migration
   hot_migration
   test
   exc
   utils

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

