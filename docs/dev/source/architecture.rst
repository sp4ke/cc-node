Architecture
============

Start up proccess
-----------------

See ``bin/cc-node``.

Summary of the steps:

Binary:

* First parse command line options and configuration file
* Switch to a daemon context
* Instanciate and launch a :class:`cloudcontrol.node.node.MainLoop`


Organisation of modules/packages
--------------------------------

.. tree ../../ccnode | grep -v \.pyc | grep -v \\.\\.

.. code-block:: text

    |-- config.py
    |-- exc.py
    |-- host
    |   |-- __init__.py
    |   |-- tags.py
    |-- hypervisor
    |   |-- domains
    |   |   |-- __init__.py
    |   |   |-- vm_tags.py
    |   |-- __init__.py
    |   |-- jobs.py
    |   |-- lib.py
    |   |-- tags.py
    |-- __init__.py
    |-- jobs.py
    |-- node.py
    |-- plugins.py
    |-- tags.py
    |-- utils.py

TODO

Node
----

.. automodule:: cloudcontrol.node.node
    :members:
