Test scenario for cc-node
=========================

Host
----

* Check all tags
* Check handlers: shutdown, execute_command


Hypervisor
----------

* Check all tags
* Check handlers: vm_start, vm_stop (+destroy), vm_suspend, vm_resume, vm_undefine,
  vm_export, vm_define
* Check statuses of VMs change
* Check objects are registered/unregistered to the cc-server when VMs
  disapear/appear.

Error handling
..............

Behaviour when libvirt connection is lost:

    * libvirtstatus tag is updated to disconnected
    * domains are unregistered
    * some tags (relative to storage pools are unregistered)
    * handlers relative to VMs are removed

Behaviour when libvirt connection is  retrieved:

    * libvirtstatus tag is updated to connected
    * domains are registered
    * tags (relative to storage pools) are registered
    * handlers relative to VMs are added


CC-server
---------

Error handling
..............

Behaviour when connection is lost:

    * Nothing changes

Behaviour when connection is retrieved:

    * Authentify
    * Check role returned and load another main plugin is role changed, else
      keeps current runnning plugin
    * Register tags and objects


Error handling
--------------

* Test with libvirt/cc-server failures multiple configuration
