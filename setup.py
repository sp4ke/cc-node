#!/usr/bin/env python
#coding=utf-8

# This file is part of CloudControl.
#
# CloudControl is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CloudControl is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CloudControl.  If not, see <http://www.gnu.org/licenses/>.


import os
from setuptools import setup, find_packages

from cloudcontrol.node import __version__


ldesc = open(os.path.join(os.path.dirname(__file__), 'README')).read()


setup(
    name='cc-node',
    version=__version__,
    description='Cloud Control Node',
    long_description=ldesc,
    author='Anaël Beutot',
    author_email='anael.beutot@smartjog.com',
    license='LGPL3',
    namespace_packages=['cloudcontrol'],
    packages=find_packages(),
    scripts=['bin/cc-node'],
    data_files=(
        ('/etc/', ('etc/cc-node.conf',)),
    ),
    classifiers=[
        'Operating System :: Unix',
        'Programming Language :: Python',
    ],
)
